<?
use Bitrix\Main\Localization\Loc;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

Loc::loadMessages(__FILE__);

$arComponentDescription = array(
	'NAME'			=> Loc::getMessage('С_INTENSA_CATALOG_MENU_COMP_NAME'),
	'DESCRIPTION'	=> Loc::getMessage('С_INTENSA_CATALOG_MENU_COMP_DESCR'),
	'ICON"'			=> "/images/menu.gif",
	'CACHE_PATH'	=> "Y",
	'SORT'			=> 1,
	'PATH'			=> array(
		'ID'			=> "intensa",
		'NAME'			=> Loc::getMessage('С_INTENSA_CATALOG_MENU_INTENSA'),
		'CHILD'			=> array(
			'ID'			=> "catalog",
			'NAME'			=> Loc::getMessage('С_INTENSA_CATALOG_MENU_COMP_CATALOG'),
			'SORT'			=> 10,
		),
	),
);
?>