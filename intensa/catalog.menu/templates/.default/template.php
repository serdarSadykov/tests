<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
if(empty($arResult['SECTIONS'])) return;
?>
<nav class="c_catalog_menu navbar navbar-default">
	<div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only"><?=GetMessage('С_INTENSA_CATALOG_MENU_COMP_TOGGLE')?></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <span class="navbar-brand"><?=GetMessage('С_INTENSA_CATALOG_MENU_CATALOG')?></span>
        </div>
		
		<div class="collapse navbar-collapse">
			<ul class="nav navbar-nav">
<?
$previousLevel = 0;
foreach($arResult['SECTIONS'] as $arItem):
?>
	<?if($previousLevel && $arItem['DEPTH_LEVEL'] < $previousLevel):?>
		<?=str_repeat("</ul></li>", ($previousLevel - $arItem['DEPTH_LEVEL']));?>
	<?endif?>

		<?if($arItem['IS_PARENT']):?>
			<li class="dropdown <?=($arItem['CURRENT']?" active":"")?><?=($arItem['DEPTH_LEVEL']>1?" dropdown-submenu":"")?>">
		<?else:?>
			<li class="<?=($arItem['CURRENT']?"active ":"")?>">
		<?endif;?>
			<?if(!empty($arItem['COMP_ADDIT']['PREVIEW_PICTURE']['src'])):?>
				<a class="previewPicture thumbnail" href="<?=$arItem['COMP_ADDIT']['DETAIL_PAGE_URL']?>">
					<img src="<?=$arItem['COMP_ADDIT']['PREVIEW_PICTURE']['src']?>" alt="<?=$arItem['NAME']?>">
					<div class="caption"><?=$arItem['COMP_ADDIT']['NAME']?></div>
				</a>
			<?endif;?>
			<?if($arItem['IS_PARENT']):?>
				<a href="<?=$arItem['SECTION_PAGE_URL']?>" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
					<?=$arItem['NAME']?>
					<?if($arItem['DEPTH_LEVEL']==1):?>
						<span class="caret"></span>
					<?endif;?>
				</a>
				<ul class="dropdown-menu multi-level">
			<?else:?>
				<a href="<?=$arItem['SECTION_PAGE_URL']?>"><?=$arItem["NAME"]?></a>
			<?endif;?>
				
	<?$previousLevel = $arItem['DEPTH_LEVEL'];?>
<?endforeach?>

<?if ($previousLevel > 1):?>
	<?=str_repeat("</ul></li>", ($previousLevel-1) );?>
<?endif?>
			</ul>
		</div>
	</div>
</nav>