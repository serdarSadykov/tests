<?
use Bitrix\Main\Localization\Loc;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

Loc::loadMessages(__FILE__);

if(!\Bitrix\Main\Loader::includeModule("iblock")) return;

$arIBlock = array();
$rsIBlock = CIBlock::GetList(array('SORT' => "ASC"), array('TYPE' => $arCurrentValues['IBLOCK_TYPE'], 'ACTIVE'=>"Y"));
while($arr=$rsIBlock->Fetch())
	$arIBlock[$arr["ID"]] = "[".$arr["ID"]."] ".$arr["NAME"];

$arComponentParameters = array(
	'PARAMETERS' => array(
		'IBLOCK_ID'	=> array(
			'PARENT'			=> "BASE",
			'NAME'				=> Loc::getMessage('INTENSA_CATALOG_MENU_COMP_INFOBLOCK'),
			'TYPE'				=> "LIST",
			'ADDITIONAL_VALUES'	=> "Y",
			'VALUES'			=> $arIBlock,
			'REFRESH'			=> "Y",
		),
		'DEPTH_LEVEL' => array(
			'PARENT'			=> "BASE",
			'NAME'				=> Loc::getMessage('INTENSA_CATALOG_MENU_COMP_DEPTH_LEVEL'),
			'TYPE'				=> "LIST",
			'VALUES'			=> array(
				2=>"2",
				3=>"3",
			),
			'DEFAULT'			=> 2
		),
		'COMP_ADDIT' => array(
			'PARENT'		=> "BASE",
			'NAME'			=> Loc::getMessage('INTENSA_CATALOG_MENU_COMP_ADDIT'),
			'TYPE'			=> "CHECKBOX",
			'DEFAULT'		=> "N",
		),
		'CACHE_TIME' => array('DEFAULT' => 3600),
	)
);