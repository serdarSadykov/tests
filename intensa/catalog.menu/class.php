<?
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\SystemException;
use Bitrix\Main\Loader;
use Bitrix\Iblock\SectionTable;
use Bitrix\Iblock\ElementTable;
use Bitrix\Iblock\IblockTable;

if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

/**
 * Class CIntensaCatalogMenu
 *
 * catalog.section.list modified
 */
class CIntensaCatalogMenu extends \CBitrixComponent
{
	private $APPLICATION;
	private $SITE_DIR;
	private $CURRENT_PAGE;
	private $CURRENT_DIR;
	protected $includeComponentTemplateInCache = false;
	/**
	 * Event called from includeComponent before component execution.
	 */
	public function onIncludeComponentLang()
	{
		Loc::loadMessages(__FILE__);
	}
	/**
	 * Event called from includeComponent before component execution.
	 * Takes component parameters as argument and should return it formatted as needed.
	 *
	 * @param $params
	 * @throws Exception
	 * @return mixed
	 */
	public function onPrepareComponentParams($params)
	{
		if(empty($params['IBLOCK_ID']))
			throw new SystemException(Loc::getMessage('INTENSA_CATALOG_MENU_MODULE_NO_IBLOCK'));
		
		$params['CACHE_TIME']	= isset($params['CACHE_TIME'])?$params['CACHE_TIME']:3600;
		$params['COMP_ADDIT']	= isset($params['COMP_ADDIT'])?$params['COMP_ADDIT']:"N";
		
		$this->APPLICATION = \Bitrix\Main\Application::getInstance();
		
		$this->SITE_DIR		= SITE_DIR;
		$this->CURRENT_PAGE	= $this->APPLICATION->getContext()->getRequest()->getRequestedPage();
		$this->CURRENT_DIR	= $this->APPLICATION->getContext()->getRequest()->getRequestedPageDirectory()."/";
		
		return $params;
	}
	/**
	 * Check Required Modules
	 * @throws Exception
	 */
	protected function checkModules()
	{
		if (!Loader::includeModule('iblock'))
			throw new SystemException(Loc::getMessage('INTENSA_CATALOG_MENU_MODULE_NOT_INSTALLED', array('#NAME#' => 'iblock')));
	}
	/**
	 * Prepare data.
	 * @throws SystemException
	 */
	protected function getResult(){
		$res = IblockTable::getList(array(
			'select'=>array(
				'NAME',
				'SECTION_PAGE_URL',
				'DETAIL_PAGE_URL',
			),
			'filter'=>array('=ID' => $this->arParams['IBLOCK_ID'])
		));
		while($s_arr = $res->fetch()){
			$this->arResult = $s_arr;
		}
		
		$res = SectionTable::getList(array(
			'order' => array(
				'LEFT_MARGIN'	=>	"ASC"
			),
			'select' => array(
				'ID',
				'CODE',
				'NAME',
				'DEPTH_LEVEL',
				'IBLOCK_SECTION_ID',
			),
			'filter' => array(
				'=ACTIVE'		=> "Y",
				'=GLOBAL_ACTIVE'	=> "Y",
				'<=DEPTH_LEVEL'	=> $this->arParams['DEPTH_LEVEL'],
				'=IBLOCK_ID'		=> $this->arParams['IBLOCK_ID']
			)
		));
		
		$this->arResult['SECTIONS'] = array();
		while($s_arr = $res->fetch()){
			$s_arr['COMP_ADDIT']		= array();
			$s_arr['SECTION_PAGE_URL']	= str_replace(
				array(
					"#SITE_DIR#",
					"#ID#",
					"#CODE#",
					"#SECTION_CODE#"
				),
				array(
					($this->SITE_DIR!="/"?$this->SITE_DIR:""),
					$s_arr['ID'],
					$s_arr['CODE'],
					$s_arr['CODE']
				),
				$this->arResult['SECTION_PAGE_URL']
			);
			$this->arResult['SECTIONS'][$s_arr['ID']] = $s_arr;
		}
		
		if($this->arParams['COMP_ADDIT'] == "Y"){
			foreach($this->arResult['SECTIONS'] as &$sect){
				$res = ElementTable::getList(array(
					'order' => array(
						'SHOW_COUNTER'	=>	"DESC"
					),
					'select' => array(
						'ID',
						'CODE',
						'IBLOCK_ID',
						'PREVIEW_PICTURE',
						'DETAIL_PICTURE',
						'NAME'
					),
					'filter' => array(
						'=SECTION.IBLOCK_SECTION_ID' => $sect['ID']
					),
					'runtime' => array(
						'SECTION' => array(
							'data_type' => '\Bitrix\Iblock\SectionElementTable',
							'reference' => array(
								'=this.ID' => 'ref.IBLOCK_ELEMENT_ID'
							),
							'join_type' => 'left'
						),
					),
					'limit'	=> 1
				));
				while($s_arr = $res->fetch()){
					$s_arr['DETAIL_PAGE_URL'] = str_replace(
						array(
							"#SITE_DIR#",
							"#ID#",
							"#CODE#",
							"#ELEMENT_CODE#",
							"#SECTION_ID#",
							"#SECTION_CODE#"
						),
						array(
							($this->SITE_DIR!="/"?$this->SITE_DIR:""),
							$s_arr['ID'],
							$s_arr['CODE'],
							$s_arr['CODE'],
							$sect['ID'],
							$sect['CODE']
						),
						$this->arResult['DETAIL_PAGE_URL']
					);
					if(empty($s_arr['PREVIEW_PICTURE']) && !empty($s_arr['DETAIL_PICTURE'])){
						$s_arr['PREVIEW_PICTURE'] = $s_arr['DETAIL_PICTURE'];
					}
					if($s_arr['PREVIEW_PICTURE']){
						$s_arr['PREVIEW_PICTURE'] = CFile::ResizeImageGet($s_arr['PREVIEW_PICTURE'], array('width'=>150, 'height'=>150), BX_RESIZE_IMAGE_PROPORTIONAL, true);
					}
					$sect['COMP_ADDIT'] = $s_arr;
				}
			}
			unset($sect);
		}
	}
	/**
	 * Check current item.
	 */
	public function checkCurrent(){
		$prev_sect = NULL;
		foreach(array_reverse($this->arResult['SECTIONS']) as $sect){
			$this->arResult['SECTIONS'][$sect['ID']]['IS_PARENT'] = $prev_sect && $prev_sect['DEPTH_LEVEL'] > $sect['DEPTH_LEVEL'];
			
			$this->arResult['SECTIONS'][$sect['ID']]['CURRENT'] =
				$this->arResult['SECTIONS'][$sect['ID']]['CURRENT']					||
				strpos($this->CURRENT_DIR,	$sect['SECTION_PAGE_URL']) !== false	||
				strpos($this->CURRENT_PAGE,	$sect['SECTION_PAGE_URL']) !== false;

			if($sect['IBLOCK_SECTION_ID'] && $this->arResult['SECTIONS'][$sect['ID']]['CURRENT']){
				$this->arResult['SECTIONS'][$sect['IBLOCK_SECTION_ID']]['CURRENT'] = true;
			}
			$prev_sect = $sect;
		}
		unset($sect);
	}
	public function executeComponent()
	{
		$this->checkModules();
		try{
			if($this->StartResultCache()){
				$this->getResult();
				if($this->includeComponentTemplateInCache){
					$this->checkCurrent();
					$this->includeComponentTemplate();
				}else{
					$this->endResultCache();
				}
			}
			if(!$this->includeComponentTemplateInCache){
				$this->checkCurrent();
				$this->IncludeComponentTemplate();
			}
		}catch(SystemException $e){
			ShowError($e->getMessage());
		}
	}
}
?>
