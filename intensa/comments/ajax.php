<?
define('STOP_STATISTICS',	true);
define('NO_AGENT_CHECK',	true);
define('PUBLIC_AJAX_MODE',	true);

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Highloadblock\HighloadBlockTable;

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

if(!check_bitrix_sessid() || !Loader::includeModule('highloadblock')) die();

Loc::loadMessages(__FILE__);

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
	$result = array('CODE'=>"ERROR",'MESSAGE'=>Loc::getMessage('INTENSA_COMMENTS_MODULE_DEFAULT_ERROR'),'CONTENT'=>NULL);
	try{
		if(empty($_POST['UF_DESCR'])) throw new Exception(Loc::getMessage('INTENSA_COMMENTS_MODULE_NEED_DESCR'));
		
		$fields = array(
			'UF_DESCR'		=> strip_tags($_POST['UF_DESCR']),
			'UF_LINKING'	=> isset($_POST['UF_LINKING'])	?strip_tags($_POST['UF_LINKING'])	:"",
			'UF_NAME'		=> isset($_POST['UF_NAME'])		?strip_tags($_POST['UF_NAME'])		:"",
			'UF_LAST_NAME'	=> isset($_POST['UF_LAST_NAME'])?strip_tags($_POST['UF_LAST_NAME'])	:"",
			'UF_EMAIL'		=> isset($_POST['UF_EMAIL'])	?strip_tags($_POST['UF_EMAIL'])		:"",
			'UF_DATETIME'	=> new \Bitrix\Main\Type\DateTime(),
		);

		$hlblock = HighloadBlockTable::getList(array(
			'filter' => array(
				'TABLE_NAME' => "c_intensa_comments",
			))
		)->fetch();
		$entity		= HighloadBlockTable::compileEntity($hlblock);
		$dataClass	= $entity->getDataClass();
		$cresult 	= $dataClass::add($fields);
		if(!$cresult->isSuccess()) throw new Exception(Loc::getMessage('INTENSA_COMMENTS_MODULE_NEED_DESCR'));


		$fields['UF_DATETIME']	= $fields['UF_DATETIME']->toString();
		$result['CODE']			= "OK";
		$result['CONTENT']		= $fields;
		$result['MESSAGE']		= '<div class="alert alert-success">Ваш комментарий добавлен</div>';

		BXClearCache(true, "/".SITE_ID."/intensa/comments/");
	}catch(Exception $exc){
		$result['MESSAGE'] = $exc->getMessage();
	}
	echo Bitrix\Main\Web\Json::encode($result);
}
require_once($_SERVER['DOCUMENT_ROOT'].BX_ROOT.'/modules/main/include/epilog_after.php');
die();