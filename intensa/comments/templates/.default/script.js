$(function(){
	var intensa_comments = $('.intensa_comments');
	intensa_comments.find('form').submit(function(e){
		e.preventDefault();
		var form	= $(this);
		var control	= form.find('button[type=submit]');
		var input	= form.find("input,textarea");
		var formData = {};
		var hasError= false;
		input.each(function(){
			var _this = $(this);
			if(!_this.val() && _this.hasClass("required")){
				inputError(_this);
				hasError = true;
			}
			formData[_this.attr("name")] = _this.val();
		});
		if(hasError) return;
		
		input.prop("disabled",true);
		control.prop("disabled",true).data('html',control.html()).text("Отправляем");
		$.post(form.attr('action'),formData,function(data){
			if(data.CODE == "OK"){
				form.html(data.MESSAGE);
				appendComment(data.CONTENT);
			}else{
				cerror(data.MESSAGE);
			}
		},'json').fail(function(as){
			cerror("Произошла ошибка, попробуйте позже");
		}).always(function(){
			input.prop("disabled",false);
			control.prop("disabled",false).html(control.data('html'));
		});
	});

	function appendComment(fields){
		intensa_comments.find('.comments').prepend(
			$('<div>').addClass('panel panel-default').append(
				$('<div>').addClass('panel-heading').append(
					$('<div>').addClass('row').append(
						$('<div>').addClass('col-sm-6').append(
							$('<div>').addClass('panel-title').text(fields['UF_NAME']+" "+fields['UF_LAST_NAME']).append(
								$('<small>').addClass('text-muted').text(fields['UF_MAIL'])
							)
						),
						$('<div>').addClass('col-sm-2'),
						$('<div>').addClass('col-sm-4').append(
							$('<small>').addClass('text-muted pull-right').text(fields['UF_DATETIME'])
						)
					)
				),
				$('<div>').addClass('panel-body').append(fields['UF_DESCR'])
			)
		);
	}
	function inputError(target){
		target.parent().addClass("has-error");
		setTimeout(function(){
			target.parent().removeClass("has-error");
		},1000);
	}
		
	function cerror(text, title){
		title = title || "Ошибка";
		var errorM = intensa_comments.find('.errorModal');
		errorM.find(".val").text(text);
		errorM.find(".modal-title").text(title);
		errorM.modal('show');
	}
	function cnotif(text, title){
		title = title || "Внимание";
		var notifModal = intensa_comments.find('.notifModal');
		notifModal.find(".val").html(text);
		notifModal.find(".modal-title").html(title);
		notifModal.modal('show');
	}
});


