<?
use Bitrix\Main\Localization\Loc;
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
Loc::loadMessages(__FILE__);
?>
<div class="intensa_comments">
	<h2><?=Loc::getMessage('INTENSA_COMMENTS_COMP_LEAVE_COMMENT')?></h2>
	<form action="<?=$arResult['ACTION_URL']?>" method="POST" class="form">
		<?foreach($arParams['FIELDS'] as $field):?>
			<div class="form-group">
				<label><?=Loc::getMessage('INTENSA_COMMENTS_COMP_FIELDS_'.$field)?></label>
				<input type="text" name="<?='UF_'.$field?>" class="form-control required" required/>
			</div>
		<?endforeach;?>
		<div class="form-group">
			<label><?=Loc::getMessage('INTENSA_COMMENTS_COMP_FIELDS_UF_DESCR')?></label>
			<textarea name="UF_DESCR" class="form-control required" required></textarea>
		</div>
		<button type="submit" class="btn btn-primary"><?=Loc::getMessage('INTENSA_COMMENTS_COMP_SAVE')?> <span class="glyphicon glyphicon-ok"></span></button>
		<input type="hidden" name="UF_LINKING" value="<?=$arParams['ID']?>">
		<?=bitrix_sessid_post()?>
	</form>
	<div class="comments">
		<?foreach($arResult['COMMENTS'] as $comment):?>
		<div class="panel panel-default">
			<div class="panel-heading">
				<div class="row">
					<div class="col-sm-6">
						<?if(!empty($comment['UF_NAME'])):?>
							<div class="panel-title">
								<?=$comment['UF_NAME']." ".$comment['UF_LAST_NAME']?>
							<?if(!empty($comment['UF_EMAIL'])):?>
								<small class="text-muted">(<?=$comment['UF_EMAIL']?>)</small>
							<?endif;?>
							</div>
						<?endif;?>
					</div>
					<div class="col-sm-2">
					</div>
					<div class="col-sm-4">
						<?if(!empty($comment['UF_DATETIME'])):?>
							<small class="pull-right text-muted"><?=$comment['UF_DATETIME']?></small>
						<?endif;?>
					</div>
				</div>
			</div>
			<div class="panel-body">
				<?=$comment['UF_DESCR']?>
			</div>
		</div>
		<?endforeach;?>
	</div>
	
	<div class="modal fade errorModal" tabindex="-1" role="dialog">
	  <div class="modal-dialog modal-sm">
		<div class="modal-content">

		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<div class="modal-title">????????</div>
		  </div>
		  <div class="modal-body">
			<div class="val"></div>
		  </div>
		</div>
	  </div>
	</div>
	<div class="modal fade notifModal" tabindex="-1" role="dialog">
	  <div class="modal-dialog modal-md">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<div class="modal-title">????????</div>
		  </div>
		  <div class="modal-body">
			<div class="val"></div>
		  </div>
		</div>
	  </div>
	</div>
</div>

