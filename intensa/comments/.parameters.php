<?
use Bitrix\Main\Localization\Loc;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

Loc::loadMessages(__FILE__);

$arComponentParameters = array(
	'PARAMETERS' => array(
		'FIELDS'	=> array(
			'PARENT'			=> "BASE",
			'NAME'				=> Loc::getMessage('INTENSA_COMMENTS_COMP_FIELDS'),
			'TYPE'				=> "LIST",
			'ADDITIONAL_VALUES'	=> "N",
			'MULTIPLE'			=> "Y",
			'COLS'				=> 3,
			'VALUES'			=> array(
				'NAME'		=> Loc::getMessage('INTENSA_COMMENTS_COMP_FIELDS_NAME'),
				'LAST_NAME'	=> Loc::getMessage('INTENSA_COMMENTS_COMP_FIELDS_LAST_NAME'),
				'EMAIL'		=> Loc::getMessage('INTENSA_COMMENTS_COMP_FIELDS_EMAIL'),
			),
		),
		'ID' => array(
			'PARENT'	=> "BASE",
			'NAME'		=> Loc::getMessage('INTENSA_COMMENTS_COMP_ID'),
			'TYPE'		=> "STRING",
			'MULTIPLE'	=> "N",
			'DEFAULT'	=> '={$_REQUEST["ID"]}',
			'COLS'		=> 5,
		),
		'CACHE_TIME' => array('DEFAULT' => 3600),
	)
);