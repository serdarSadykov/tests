<?
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\SystemException;
use Bitrix\Main\Loader;
use Bitrix\Main\Application;
use Bitrix\Highloadblock\HighloadBlockTable;


if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

/**
 * Class CIntensaComments
 *
 * View comments from HL
 */
class CIntensaComments extends \CBitrixComponent
{
	private $APPLICATION;
	private $SITE_DIR;
	private $CURRENT_PAGE;
	const hlName		= "CIntensaComments";
	const tableName		= "c_intensa_comments";
	const hlFields		= array(
		'UF_LINKING',
		'UF_NAME',
		'UF_LAST_NAME',
		'UF_EMAIL',
		'UF_DESCR',
		'UF_DATETIME',
	);
	protected $includeComponentTemplateInCache = false;
	/**
	 * Event called from includeComponent before component execution.
	 */
	public function onIncludeComponentLang()
	{
		Loc::loadMessages(__FILE__);
	}
	/**
	 * Event called from includeComponent before component execution.
	 * Takes component parameters as argument and should return it formatted as needed.
	 *
	 * @param $params
	 * @throws Exception
	 * @return mixed
	 */
	public function onPrepareComponentParams($params)
	{
		$this->APPLICATION	= Application::getInstance();
		
		$this->SITE_DIR		= SITE_DIR;
		$this->CURRENT_PAGE	= $this->APPLICATION->getContext()->getRequest()->getRequestedPage();


		$params['CACHE_TIME']	= isset($params['CACHE_TIME'])	?$params['CACHE_TIME']		:3600;
		$params['FIELDS']		= isset($params['FIELDS'])		?$params['FIELDS']			:array();
		$params['ID']			= isset($params['ID'])			?"ELEMENT_".$params['ID']	:$this->CURRENT_PAGE;
		
		
		return $params;
	}
	/**
	 * Check Required Modules
	 * @throws Exception
	 */
	protected function checkModules()
	{
		if (!Loader::includeModule('highloadblock'))
			throw new SystemException(Loc::getMessage('INTENSA_COMMENTS_MODULE_NOT_INSTALLED', array('#NAME#' => 'highloadblock')));
	}
	/**
	 * Proceed actions.
	 */
	protected function proceedActions(){}
	/**
	 * Prepare data.
	 * @throws SystemException
	 */
	protected function getResult(){
		$this->arResult['COMMENTS']		= array();
		$this->arResult['ACTION_URL']	= $this->getPath()."/ajax.php";

		$hlblock	= HighloadBlockTable::getById($this->checkGetHighLoadBlock())->fetch();
		$entity		= HighloadBlockTable::compileEntity($hlblock);
		$dataClass	= $entity->getDataClass();

		$result = $dataClass::getList(array(
			'select'=> array('*'),
			'order'	=> array('UF_DATETIME'=>"DESC"),
			'filter'=> array('=UF_LINKING'=>$this->arParams['ID'])
		));

		while($row = $result->fetch()){
			$this->arResult['COMMENTS'][$row['ID']] = $row;
		}
	}
	public static function checkGetHighLoadBlock(){
		$highBlockID = NULL;

		$hlblock = HighloadBlockTable::getList(array(
			'filter' => array(
				'TABLE_NAME' => self::tableName,
			))
		)->fetch();
		if($hlblock){
			$highBlockID = $hlblock['ID'];
		}else{
			$result = HighloadBlockTable::add(array(
				'NAME'		=> self::hlName,
				'TABLE_NAME'=> self::tableName,
			));
			if(!$result->isSuccess())
				throw new SystemException(Loc::getMessage('INTENSA_COMMENTS_MODULE_CANT_ADD_HL')."2");

			$highBlockID = $result->getId();

			$oUserTypeEntity = new \CUserTypeEntity();
			foreach(self::hlFields as $field){
				$aUserField = array(
					'ENTITY_ID'			=> 'HLBLOCK_'.$highBlockID,
					'FIELD_NAME'		=> $field,
					'USER_TYPE_ID'		=> "string",
					'SORT'				=> 500,
					'MULTIPLE'			=> "N",
					'MANDATORY'			=> "N",
					'SHOW_FILTER'		=> "N",
					'SHOW_IN_LIST'		=> "Y",
					'EDIT_IN_LIST'		=> "Y",
					'IS_SEARCHABLE'		=> "N",
					'SETTINGS'			=> array(),
				);
		 
				if(!$oUserTypeEntity->Add($aUserField))
					throw new SystemException(Loc::getMessage('INTENSA_COMMENTS_MODULE_CANT_ADD_HL'));
			}
		}
		return $highBlockID;
	}
	/**
	 * Function calls __includeComponent in order to execute the component.
	 */
	public function executeComponent()
	{
		$this->checkModules();
		$this->proceedActions();
		try{
			if($this->StartResultCache()){
				$this->getResult();
				if($this->includeComponentTemplateInCache){
					$this->includeComponentTemplate();
				}else{
					$this->endResultCache();
				}
			}
			if(!$this->includeComponentTemplateInCache){
				$this->IncludeComponentTemplate();
			}
		}catch(SystemException $e){
			ShowError($e->getMessage());
		}
	}
}
?>
